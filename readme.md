#Laravel 5.4 Entrust

#####Laravel 5.4 Entrust Auth Roles and Permissions Skeleton app.

Laravel User auth (make:auth) and Entrust roles system with user registration disabled.

Roles and Permissions can be seen at Database/seeds files.

On every login/logout will create a log entry auth_log table.

##Requisites
* mysql
* redis

##Install

```sh
$ git clone git@bitbucket.org:okahei/laravel-5.4-entrust.git
$ cd laravel-5.4-entrust/
$ composer install
```

####Configure Database/Session:
> Copy .env.example to .env and edit:

* Mysql
    * DB_DATABASE=DatabaseName
    * DB_USERNAME=username
    * DB_PASSWORD=password
* Redis
    * SESSION_DRIVER=redis

> Create mysql database "DatabaseName"

#### generate Key
```sh
$ php artisan key:generate
```

###Migrate and Seed Roles, Permissions, Users
```sh
$ php artisan migrate
$ php artisan db:seed --class=DatabaseSeeder
```

##Test
Simple test, login as admin and test if we can see an admin link.
```sh
$ composer test
```

##Start App
```sh
$ php artisan serve
```

Open in Browser http://localhost:8000 and login

_**Username**: superman@localhost.com_

_**Password**: 123456_