@extends('layouts.app')

@section('content')
    <div class="container">
        {{ $activities->links() }}
        <div class="list-group">
            @include('activity.list')
        </div>
    </div>
@stop
