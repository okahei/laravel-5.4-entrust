@foreach($activities as $activity)
    <li class="list-group-item">
        @include("activity.types.{$activity->name}")
    </li>
@endforeach