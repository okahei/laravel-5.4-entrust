<h4>{{ $activity->user ? ucfirst($activity->user->name): 'Console User' }}</h4> <strong>Update</strong> a User:
@if(isset($activity->subject->name ))
    <strong>{{ str_limit($activity->subject->name, 22) }}</strong>
@else
    DELETED
@endif

<p>{{ $activity->created_at->diffForHumans() }}</p>