<h4>{{ $activity->user ? ucfirst($activity->user->name): 'Console User' }}</h4> <strong>Update</strong> a Product:
@if(isset($activity->subject->title ))
    <strong>{{ str_limit($activity->subject->title, 22) }}</strong>
@else
    DELETED
@endif

<p>{{ $activity->created_at->diffForHumans() }}</p>