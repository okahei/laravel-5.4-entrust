@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        Admin Index

                        <a href="{{ route('users') }}">Users</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
