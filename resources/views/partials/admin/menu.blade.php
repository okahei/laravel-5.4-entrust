<li class="dropdown">
    <a href="#" class="dropdown-toggle"
       data-toggle="dropdown"
       role="button"
       aria-haspopup="true"
       aria-expanded="false">
        Management
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li><a href="{{route('users.index')}}">Users</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="{{route('admin.activity')}}">Activity</a></li>
    </ul>
</li>