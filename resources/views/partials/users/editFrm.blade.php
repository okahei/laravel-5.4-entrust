{!! Form::model($user,['method' => 'PATCH',
                       'url' => route('users.update', ['id' => $user->id])])!!}

@include('partials.users.frm', ['submit' => 'Edit User'])

{!! Form::close() !!}