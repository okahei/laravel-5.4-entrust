<div class="form-group">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'Email') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('password', 'Password') !!}
    {!! Form::text('password', '', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    @include('partials.users.rolesSelect')
</div>

<div class="form-group">
    <h4>Permissions</h4>
    <div class="list-group" id="user_permissions">
        @foreach($permissions as $permission)
            <li class="list-group-item">{{ $permission->name }}</li>
        @endforeach
    </div>
</div>

<button class="btn btn-success" type="submit">
    {{$submit}}
</button>