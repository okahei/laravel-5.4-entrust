<div id="deleteModalConfirmation" class="modal fade"
     tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">

    <div class="modal-dialog modal-sm" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
               <h2 class="text-center text-info">Confirmation Please</h2>
            </div>

            <div class="modal-body">
                @include('partials.users.deleteFrm')
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>