{!! Form::label('Roles', 'Roles') !!}
{!! Form::select('roles',
                 $roles ,
                 count($user->roles()->get()) > 0 ? $user->roles()->get()[0]->id : null,
                 ['class' => 'form-control'])
!!}