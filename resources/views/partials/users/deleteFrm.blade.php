<div class="row">
    <div class="col-md-9 col-md-offset-3">
        {!! Form::model($user,
                                [
                                  'method' => 'DELETE',
                                  'url' => route('users.destroy', ['id' => $user->id])
                                ])
                               !!}
        <button class="btn btn-danger btn-lg" type="submit">
            Delete User
        </button>
        {!! Form::close() !!}

    </div>
</div>