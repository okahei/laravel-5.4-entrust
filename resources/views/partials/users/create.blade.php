{!! Form::open(['method' => 'POST', 'url' => route('users.store')])!!}

@include('partials.users.frm', ['submit' => 'Create User'])

{!! Form::close() !!}