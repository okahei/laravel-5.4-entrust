<a href="{{route('users.show',['id' => $user->id])}}" class="list-group-item">
    <h4>{{ $user->name }}</h4>
    <p>Email: {{ $user->email }}</p>
    @each('partials.users.roles.item', $user->roles()->get(), 'role', 'partials.users.roles.empty')
</a>