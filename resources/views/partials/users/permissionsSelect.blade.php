{!! Form::label('Permissions', 'Permissions') !!}
{!! Form::select(
                'Permissions',
                $permissions ,
                $user->getPermissionsIds(),
                ['multiple' => true],
                ['class' => 'form-control']
    )
                !!}