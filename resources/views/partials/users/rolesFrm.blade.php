{!! Form::model($user, ['url' => route('admin.userRoles.update',['id' => $user->id]),
 'class' => 'form']) !!}

<div class="form-group">
    @include('partials.users.rolesSelect')
</div>

<div class="form-group">
    <h4>Permissions</h4>
    <div class="list-group" id="user_permissions">
        @foreach($permissions as $permission)
            <li class="list-group-item">{{ $permission->name }}</li>
        @endforeach
    </div>
</div>

<button class="btn btn-success" type="submit">
    Edit User
</button>

{!! Form::close() !!}