@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        You are logged in!

                        <h2>User Roles</h2>
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(Auth::user()->roles()->get() as $role)
                                <tr>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->display_name }}</td>
                                    <td>{{ $role->description }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <h2>User Permissions</h2>
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(Auth::user()->getAllPermissions() as $perm)
                                <tr>
                                    <td>{{ $perm->name }}</td>
                                    <td>{{ $perm->display_name }}</td>
                                    <td>{{ $perm->description }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>



                        @role('admin')
                        <p>This is visible to users with the admin role. Gets translated to
                            \Entrust::role('admin')</p>
                        @endrole

                        @permission('manage-admins')
                        <p>This is visible to users with the given permissions. Gets translated to
                            \Entrust::can('manage-admins'). The \@ can directive is already taken by core
                            laravel authorization package, hence the \@ permission directive instead.</p>
                        @endpermission

                        @ability('admin,owner', 'create-post,edit-user')
                        <p>This is visible to users with the given abilities. Gets translated to
                            \Entrust::ability('admin,owner', 'create-post,edit-user')</p>
                        @endability
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
