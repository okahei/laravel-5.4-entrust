@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="text-info">Users List</h3>
                            </div>
                            <div class="col-md-6">
                                <br>
                                <a class="btn btn-info pull-right"
                                   href="{{route('users.create')}}">
                                    Create User
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        {{ $users->links() }}
                        <div class="list-group">
                            @each('partials.users.listItem', $users, 'user')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
