@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">
                                <h2>
                                    {{ $user->email }}
                                </h2>
                                <a class="btn btn-info"
                                   href="{{route('admin.user.activity', ['name' => $user->name])}}">
                                    User Activity
                                </a>
                            </div>
                            <div class="col-md-3">
                                <br>
                                <a href="{{route('users.edit',['user' => $user->id])}}" class="btn btn-info">
                                    Edit
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        User Profile...
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script type="text/javascript">
        //permissions list group selector
        var permsList = $('#user_permissions');
        /**
         * Construct Url
         * /foo/bar/_var_/baz -> /foo/bar/2/baz
         *
         * @param roleId
         * @returns {string}
         */
        function setRouteUrl(roleId) {
            var baseUrl = "{{route('users.roles.permissions', ['role' => '_role_'] )}}";
            return baseUrl.replace(/_.*_/g, roleId)
        }

        /**
         * On roles Select change update Permissions List-group items
         */
        $("select[name='roles']").on('change', function () {
            var url = setRouteUrl(this.value);
            $.ajax({
                type: "GET",
                dataType: "json",
                url: url
            }).done(function (data, textStatus, jqXHR) {
                permsList.empty();
                data.forEach(function (permission) {
                    permsList
                            .append('<li class="list-group-item">' + permission.display_name + '</li>');
                })
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus);
            });
        });

    </script>
@endsection

