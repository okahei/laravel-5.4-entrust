<?php

namespace Tests\Feature;

use Tests\BrowserKitTestCase;
/*use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;*/
use App\User;

class RoleTest extends BrowserKitTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testManagementLinkShowedToDesiredGroups()
    {
        $user = new User;
        $superman = $user::where('name', 'superman')->first();

        $this->actingAs($superman)
            ->get('/home')->see('Management');
    }
}
