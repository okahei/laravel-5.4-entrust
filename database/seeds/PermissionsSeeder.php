<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createArticle = new Permission();
        $createArticle->name         = 'create-article';
        $createArticle->display_name = 'Create Articles'; // optional
        $createArticle->description  = 'create new Articles in CRM'; // optional
        $createArticle->save();

        $editUser = new Permission();
        $editUser->name         = 'edit-user';
        $editUser->display_name = 'Edit Users'; // optional
        $editUser->description  = 'edit existing users'; // optional
        $editUser->save();

        $editAdminUsers = new Permission();
        $editAdminUsers->name         = 'edit-admin-user';
        $editAdminUsers->display_name = 'Edit Admin Users'; // optional
        $editAdminUsers->description  = 'edit existing Admin users'; // optional
        $editAdminUsers->save();

        $sellArticle = new Permission();
        $sellArticle->name         = 'sell-article';
        $sellArticle->display_name = 'Sell Articles'; // optional
        $sellArticle->description  = 'Sell Articles to clients'; // optional
        $sellArticle->save();


        /*attach perms*/
        $adminRole = $this->getRole(1);
        $adminRole->attachPermissions(array($createArticle, $editAdminUsers, $editUser));

        $managerRole = $this->getRole(2);
        $managerRole->attachPermissions(array($createArticle, $editUser, $sellArticle));

        $normalUserRole =  $this->getRole(3);
        $normalUserRole->attachPermission($sellArticle);
    }

    /**
     * Get Role by Id
     *
     * @param $id
     * @return mixed
     */
    private function getRole($id){
        return (new App\Role)::findOrFail($id);
    }
}
