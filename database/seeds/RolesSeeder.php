<?php


use Illuminate\Database\Seeder;
use App\Role;
use App\User;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description  = 'User is allowed to manage and edit other users'; // optional
        $admin->save();

        $manager = new Role();
        $manager->name         = 'manager';
        $manager->display_name = 'User Manager'; // optional
        $manager->description  = 'User is allowed to manage normal users'; // optional
        $manager->save();

        $normal_user = new Role();
        $normal_user->name         = 'normal_user';
        $normal_user->display_name = 'Normal User'; // optional
        $normal_user->description  = 'User is allowed to use app'; // optional
        $normal_user->save();

        /*attach roles*/
        /*$superman = $this->getUser(1);
        $superman->attachRole($admin);

        $managerUser = $this->getUser(3);
        $managerUser->attachRole($manager);

        $normalUser =  $this->getUser(2);
        $normalUser->attachRole($normal_user);*/
    }


    /**
     * Get User by Id
     *
     * @param $id
     * @return mixed
     */
    private function getUser($id){
        return (new User)::findOrFail($id);
    }
}
