<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    private $password = '123456';
    private $roles;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $adminRole = $this->getRole(1);
        $managerRole = $this->getRole(2);
        $normalUserRole =  $this->getRole(3);

        $this->roles = [$adminRole,$managerRole,$normalUserRole];
        
        $user = new User;
        $user->name = 'superman';
        $user->email = 'superman@localhost.com';
        $user->password = Hash::make($this->password);
        $user->save();
        $user->attachRole($adminRole);

        $user = new User;
        $user->name = 'luser';
        $user->email = 'luser@localhost.com';
        $user->password = Hash::make($this->password);
        $user->save();
        $user->attachRole($normalUserRole);

        $user = new User;
        $user->name = 'foo';
        $user->email = 'foo-manager@localhost.com';
        $user->password = Hash::make($this->password);
        $user->save();
        $user->attachRole($managerRole);

        factory(App\User::class, 50)->create()->each(function ($u) {
            $u->attachRole($this->roles[array_rand($this->roles,1)]);
        });
    }

    /**
     * Get Role by Id
     *
     * @param $id
     * @return mixed
     */
    private function getRole($id){
        return (new App\Role)::findOrFail($id);
    }
}
