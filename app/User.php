<?php

namespace App;

use App\Traits\RecordsActivity;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use RecordsActivity;
    use EntrustUserTrait;
    use Notifiable;

    /**
     * Validate Rules
     * 
     * @return array
     */
    public static function rules(){
        return [
            'name' => 'required|min:3|max:15',
            'email' => 'required|email|unique:users,email',
            'password' => 'nullable|min:8|max:25'
        ];
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get all perms
     *
     * @return array
     */
    public function getAllPermissions()
    {

        $perms = [];
        $roles = $this->roles()->get();

        foreach ($roles as $role) {
            foreach($role->perms as $perm){
                array_push($perms,$perm);
            }
        }
        return $perms;
    }

    /**
     * Get an Array of User's Permissions Ids
     * 
     * @return array
     */
    public function getPermissionsIds(){
        $ids = [];
        $perms = $this->getAllPermissions();
        foreach($perms as $perm){
            array_push($ids, $perm->id);
        }
        return $ids;
    }

    /**
     * Return Users's Activity
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activity()
    {
        return $this->hasMany('App\Activity');
    }

    /**
     * Return Users's Activity
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authlog()
    {
        return $this->hasMany('App\AuthLog');
    }
    
}
