<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('validate.user', ['only' => ['store','update']]);
    }

    /**
     * Show Admin Index
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = (new User)::paginate(10);
        return view('users.index', compact('users'));
    }

    /**
     * Edit User
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $roles = (new Role)::get()->pluck('name', 'id');
        $permissions = $user->getAllPermissions();
        return view('users.show', compact('user', 'roles', 'permissions'));
    }

    /**
     * Edit User Frm
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $roles = (new Role)::get()->pluck('name', 'id');
        $permissions = $user->getAllPermissions();
        return view('users.edit', compact('user', 'roles', 'permissions'));
    }

    /**
     * Create User Frm
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(User $user)
    {
        $roles = (new Role)::get()->pluck('name', 'id');
        $permissions = Role::first()->permissions()->get();
        return view('users.create', compact('user', 'roles', 'permissions'));
    }

    /**
     * User Store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $user = User::create(hasPasswordCryptOrRemove($request));
        $role = Role::findOrFail($request->input('roles'));
        $user->attachRole($role);
        return redirect()->to(route('users.show', ['user' => $user->id]));
    }

    /**
     * Update a User
     *
     * Validated by UserValidate Middleware
     *
     * hasPasswordCryptOrRemove Support/Helpers
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        if($request->input('roles')) {
            $role = Role::findOrFail($request->input('roles'));
            $user->detachRoles($user->roles);
            $user->attachRole($role);
        }

        $user->update(hasPasswordCryptOrRemove($request));
        flash('User Updated', 'info');
        return redirect()->back();
    }

    /**
     * Delete User
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(User $user)
    {
        //do not allow to delete it self
        if (Auth::user()->id == $user->id) {
            flash('Not Allowed to Destroy your Self', 'danger')->important();
            return redirect()->back();
        }

        //Delete User
        $user->delete();
        flash('User Deleted', 'info');
        return redirect()->route('users.index');
    }

    /**
     * Update User Role
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function userRolesUpdate(Request $request, User $user)
    {
        $role = Role::findOrFail($request->input('roles'));
        $user->detachRoles($user->roles);
        $user->attachRole($role);
        flash('User Role Update', 'info');
        return redirect()->back();
    }

    /**
     * Get permissions attached to Role
     *
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPermissionsAttachedToRole(Role $role)
    {
        return response()->json($role->permissions()->get());
    }
}