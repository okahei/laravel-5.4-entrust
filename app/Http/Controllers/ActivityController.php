<?php

namespace App\Http\Controllers;
use App\Activity;
use App\User;

class ActivityController extends Controller
{

    /**
     * ActivityController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show Activity for a user
     *
     * @param $username
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($username)
    {
        $user = User::where('name', $username)->firstOrFail();
        $activities = $user->activity()->with(['user', 'subject'])->latest()->paginate(10);
        return view('activity.user', compact('user', 'activities'));
    }

    /**
     * Get All Activities
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all()
    {
        $activities = Activity::with(['user', 'subject'])->latest()->paginate(10);
        return view('activity.all', compact('user', 'activities'));
    }
}