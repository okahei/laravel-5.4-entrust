<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Validator;

class UserValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rules = User::rules();
        //if update user profile email should throw error on duplicate,
        // update email rule with user id
        if($request->method() == 'PATCH'){
            $rules['email'] .= ',' . $request->user->id;
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        return $next($request);
    }
}