<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use ReflectionClass;
use App\Activity;

/**
 * Class RecordsActivity
 * @package App
 */
trait RecordsActivity
{
    /**
     * boot()
     */
    protected static function bootRecordsActivity()
    {
        /**
         * Entrust there are collisions with other trait methods App\User Boot
         */
        if(get_class(static::getModel()) != 'App\User')
        {
            //parent::boot();
        }
        
        foreach (static::getModelEvents() as $event) {
            static::$event(function ($model) use ($event) {
                $model->recordActivity($event);
            });
        }
    }

    /**
     * @param $event
     */
    public function recordActivity($event)
    {
        if($event == 'deleted')
        {
            Activity::create([
                'subject_id' => $this->id,
                'subject_type' => get_class($this),
                'subject_name' => static::getIdentifier($this),
                'name' => $this->getActivityName($this, $event),
                'user_id' => Auth::user() ? Auth::user()->id : 0,
            ]);
        } else {
            Activity::create([
                'subject_id' => $this->id,
                'subject_type' => get_class($this),
                'subject_name' => static::getIdentifier($this),
                'name' => $this->getActivityName($this, $event),
                'user_id' => Auth::user() ? Auth::user()->id : 0,
            ]);
        }

    }

    /**
     * Try to figure if $model has one of theese Propertry
     * name/username/title
     * @param $model
     * @return null
     */
    public static function getIdentifier($model)
    {
        $identifier = null;

        if(isset($model->name))
        {
            $identifier = $model->name;
        }
        elseif(isset($model->username))
        {
            $identifier = $model->username;
        }
        elseif(isset($model->title)){

            $identifier = $model->title;
        }

        return $identifier;

    }
    /**
     * @param $model
     * @param $action
     * @return string
     */
    protected function getActivityName($model, $action)
    {
        $name = strtolower((new ReflectionClass($model))->getShortName());

        return "{$action}_{$name}";
    }

    /**
     * @return array
     */
    protected static function getModelEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }

        return [
            'created', 'deleted', 'updated'
        ];
    }
}