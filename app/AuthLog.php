<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'event'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
