<?php

namespace App\Listeners;
use App\AuthLog;

class UserEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function onUserLogin($event) {
        AuthLog::create([
            "user_id" => $event->user->id,
            "event"   => "login"
        ]);
    }
    
    
    /**
     * Handle user logout events.
     */
    public function onUserLogout($event) {
        AuthLog::create([
            "user_id" => $event->user->id,
            "event"   => "logout"
        ]);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@onUserLogout'
        );
    }

}