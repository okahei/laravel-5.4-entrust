<?php

if (!function_exists('hasPasswordCryptOrRemove')) {

    /**
     * Update User request has password ?
     *
     * @param $request
     * @return mixed
     */
    function hasPasswordCryptOrRemove($request)
    {
        if ($request->has('password')) {
            $password = bcrypt($request->input('password'));
            $user_data = $request->all();
            $user_data['password'] = $password;
        } else {
            $user_data = $request->except(['password']);
        }
        return $user_data;
    }
}